(function() {
    'use strict';
    var gulp = require('gulp');
    var jshint = require('gulp-jshint');
    var minimist = require('minimist');
    var rename = require('gulp-rename');
    var del = require('del');
    var cssMin = require('gulp-clean-css');
    var htmlMin = require('gulp-htmlmin');
    var jsMin = require('gulp-uglify');
    var concat = require('gulp-concat');
    var sourcemaps = require('gulp-sourcemaps');
    var merge = require('merge-stream');
    var gutil = require('gulp-util');
    var shell = require('gulp-shell');
    var cache = require('gulp-cached');
    var flatten = require('gulp-flatten');
    var sass = require('gulp-sass');
    var autoprefixer = require('gulp-autoprefixer');
    var replace = require('gulp-replace');
    var jsdoc = require('gulp-jsdoc3');
    // minimist structure and defaults for this task configuration
    var knownOptions = {
        string: ['name', 'message']
    };
    var options = minimist(process.argv.slice(2), knownOptions);
    // The root working directory where code is edited
    var srcRoot = 'src/';
    var docsRoot = 'docs/';
    // The root destination directory for tests
    var dstRoot = 'dist/';
    var buildDst = 'build/';

    function lint() {
        return gulp.src(srcRoot + '**/*.js').pipe(jshint()).pipe(jshint.reporter('jshint-stylish'));
    }

    function clean() {
        return del([dstRoot, buildDst]);
    }

    function copyUseCases() {
        var js = gulp.src([docsRoot + '**/*.module.js', docsRoot + '**/*.component.js', docsRoot + '**/*.filter.js', docsRoot + '**/*.controller.js']).pipe(sourcemaps.init()).pipe(concat('app.min.js')).pipe(jsMin()).pipe(sourcemaps.write()).pipe(flatten()).pipe(gulp.dest(buildDst));
        var html = gulp.src(docsRoot + "**/*.html").pipe(flatten()).pipe(gulp.dest(buildDst));
        return merge(js, html);
    }

    function copyAssets() {
        var bower = 'bower_components/';
        var js = gulp.src([
            bower + 'angular/angular.min.js',
            bower + 'angular/angular.min.js.map',
            bower + 'bootstrap-sass/assets/javascripts/bootstrap.min.js',
            bower + 'jquery/dist/jquery.min.js',
            bower + 'jquery/dist/jquery.min.map',
            bower + 'angular-route/angular-route.min.js',
            bower + 'angular-route/angular-route.min.js.map',
        ]).pipe(gulp.dest(buildDst));
        var css = gulp.src([
            bower + 'bootstrap/dist/css/bootstrap.min.css.map',
        ]).pipe(gulp.dest(buildDst));
        var font = gulp.src([
            bower + 'bootstrap-sass/assets/fonts/bootstrap/*.*'
        ]).pipe(gulp.dest(buildDst));
        return merge(js, font, css);
    }

    function minifyHtml() {
        return gulp.src([srcRoot + '**/*.html', srcRoot + '*.html']).pipe(sourcemaps.init()).pipe(htmlMin({
            collapseWhitespace: true
        })).pipe(sourcemaps.write()).pipe(flatten()).pipe(gulp.dest(dstRoot)).pipe(gulp.dest(buildDst));
    }

    function transMinifyScss() {
        return gulp.src([
            docsRoot + '**/*.scss', '!' + docsRoot + '**/boot*.scss',
            srcRoot + '**/*.scss'
        ]).pipe(sourcemaps.init()).pipe(concat('app.scss')).pipe(sass({
            outputStyle: 'compressed',
            precision: 8
        }).on('error', sass.logError)).pipe(flatten()).pipe(rename({
            suffix: '.min'
        })).pipe(sourcemaps.write('.')).pipe(gulp.dest(buildDst));
    }

    function transMinifyComponentScss() {
        return gulp.src([
            srcRoot + '**/*.scss'
        ]).pipe(sourcemaps.init()).pipe(concat('glam-mens-components.scss')).pipe(sass({
            outputStyle: 'compressed',
            precision: 8
        }).on('error', sass.logError)).pipe(flatten()).pipe(rename({
            suffix: '.min'
        })).pipe(sourcemaps.write('.')).pipe(gulp.dest(dstRoot));
    }

    function bootstrapDependencies() {
        var bower = 'bower_components/';
        return gulp.src([bower + 'bootstrap/dist/css/bootstrap.min.css']).pipe(replace('url(../fonts/', 'url(')).pipe(gulp.dest(buildDst));
    }

    function minifyJs() {
        return gulp.src([srcRoot + '**/*.module.js', srcRoot + '**/*.component.js', srcRoot + '**/*.filter.js', srcRoot + '**/*.controller.js', srcRoot + '**/*.directive.js', srcRoot + '**/*.factory.js', srcRoot + '**/*.service.js'])
        .pipe(sourcemaps.init()).pipe(concat('glam-mens-components.min.js'))
        .pipe(jsMin()).pipe(sourcemaps.write()).pipe(flatten()).pipe(gulp.dest(dstRoot)).pipe(gulp.dest(buildDst));
    }

    function watchHtml() {
        var watcher = gulp.watch(srcRoot + '**/*.html');
        watcher.on('all', function(event, path, stats) {
            gutil.log('File ' + path + ' was ' + event + ', running task...');
            minifyHtml();
        });
    }

    function watchScss() {
        var watcher = gulp.watch(docsRoot + '**/*.scss');
        watcher.on('all', function(event, path, stats) {
            gutil.log('File ' + path + ' was ' + event + ', running task...');
            transMinifyScss();
        });
    }

    function watchJs() {
        var watcher = gulp.watch(srcRoot + '**/*.js');
        watcher.on('all', function(event, path, stats) {
            gutil.log('File ' + path + ' was ' + event + ', running task...');
            minifyJs();
        });
    }

    function watchDocs() {
        var watcher = gulp.watch([docsRoot + '**/*.js', docsRoot + '**/*.html', docsRoot + '**/*.css']);
        watcher.on('all', function(event, path, stats) {
            gutil.log('File ' + path + ' was ' + event + ', running task...');
            copyUseCases();
        });
    }

    function tests(done) {
        gutil.log("TOBE IMPLEMENTED");
        done();
    }

    function run() {
        return gulp.src(dstRoot).pipe(shell('http-server -p 9091 ' + buildDst));
    }
    gulp.task('watch', gulp.parallel(watchHtml, watchScss, watchJs));
    gulp.task('prepare', gulp.series(gulp.parallel(clean, lint), bootstrapDependencies, copyAssets, gulp.parallel(minifyHtml, transMinifyComponentScss, transMinifyScss, minifyJs)));
    gulp.task('run-dev', gulp.series('prepare', gulp.parallel('watch')));
    gulp.task('build', gulp.series('prepare', copyUseCases, copyAssets));
    gulp.task('start', gulp.series('build', gulp.parallel(watchDocs, 'watch', run)));
})();