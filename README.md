# Glambox / Men's Market Javascript Components #

### What is this repository for? ###

* New Javascript Components for Glambox and Men's Market

### Documentation ###
* Use Cases
For an overview of this project's components, type 'gulp start' in the terminal. It will create an web-app of documentation and use cases

### How do I get set up? ###
* Summary of set up
Set-up a SSH-Key for your bitbucket account, clone this repository

* Dependencies
glam-mens-js-components, Angularjs, Jquery, Bootstrap, Angular-Route, NPM, Bower, Gulp, Git, Git Flow

* Configuration
In the project directory, run npm install and bower install. It will install the required dependencies; 

- For development of new features in this project, in the terminal type: gulp run-dev -> It will create the minified distribution files, in the folder dist. It will keep running and watching for changes on the src files, if some change happens it will re-minify and re-create the distribution files. This task is useful for changing this project the while you use it in other dependent projects.

- For development of dependent projects, you can link the local repository (i.e. folder/directory) of this project in your computer to the project of the glam-mens-js-components declared on the bower.json file of the dependent project (ref.: https://oncletom.io/2013/live-development-bower-component/)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact