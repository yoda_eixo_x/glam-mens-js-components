panels.component("panelsHeadingBodies", {
    templateUrl: "panels-heading-bodies.template.html",
    bindings: {
        panel: "<",
        heading: "<",
        body: "<",
        parentStyle: "<"
    },
    controller: function PanelsSingleBodyController() {
        var ctrl = this;
        var i;
        ctrl.onGlyphiconClick = function(onClick) {
            switch (onClick.action) {
                case "hide":
                    ctrl.body.show = false;
                    for (i = 0; i < ctrl.heading.glyphicons.length; i++) {
                        if (ctrl.heading.glyphicons[i].onClick.action === "show") {
                            ctrl.heading.glyphicons[i].show = !ctrl.heading.glyphicons[i].show;
                        } else if (ctrl.heading.glyphicons[i].onClick.action === "hide") {
                            ctrl.heading.glyphicons[i].show = !ctrl.heading.glyphicons[i].show;
                        }
                    }
                    break;
                case "delete":
                    ctrl.panel.show = false;
                    break;
                case "update":
                    break;
                case "goto":
                    window.location.href = onClick.link;
                    break;
                case "show":
                    ctrl.body.show = true;
                    for (i = 0; i < ctrl.heading.glyphicons.length; i++) {
                        if (ctrl.heading.glyphicons[i].onClick.action === "show") {
                            ctrl.heading.glyphicons[i].show = !ctrl.heading.glyphicons[i].show;
                        } else if (ctrl.heading.glyphicons[i].onClick.action === "hide") {
                            ctrl.heading.glyphicons[i].show = !ctrl.heading.glyphicons[i].show;
                        }
                    }
                    break;
            }
        };
    }
});