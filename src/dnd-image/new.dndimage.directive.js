/**
 * GLAM MENS JS COMPONENTS :: DnD Image Input Field
 * ======================================================================
 * Drag'n drop image input component.
 * ----------------------------------------------------------------------
 * @copyright   ©2017 Glambox / Men's Market
 * @since       1.0.0
 */
dndimage.directive('newDndImage', function() {
    return {
        scope: {
            fileList: '='
        },
        restrict: 'E',
        controller: NewDndImageInputController,
        controllerAs: 'ctrl',
        link: function (scope, element, attrs, ctrl) {
            // Append file input to template
            ctrl.appendItem(element);

            // Bind events
            element.on('dragover', ctrl.elementOver);
            element.on('dragleave', ctrl.elementExit);
            element.on('drop', ctrl.elementDrop);
            element.on('click', ctrl.elementClick);
            element.on('change', ctrl.elementUpdate);
        },
        template: '<div class="dnd-image"><h1>ITEMS!</h1><img ng-if="$ctrl.imageData !== \'\'" src="{{$ctrl.imageData}}" </div>'
    };
});

// Dependency injection
NewDndImageInputController.$inject = ['$scope'];

// Controller function
function NewDndImageInputController($scope) {
    /**
     * Controller handle.
     *
     * @type {NewDndImageInputController}
     */
    var $ctrl = this;

    /**
     * Handle for the base64 string that will hold the current image data.
     *
     * @type {string}
     */
    $scope.imageData = '';

    /**
     * Input element to append.
     *
     * @type {Element}
     */
    $ctrl.inputItem = document.createElement('input');

    // Set input attributes and visibility
    $ctrl.inputItem.setAttribute('type', 'file');
    $ctrl.inputItem.setAttribute('multiple', false);
    //$ctrl.inputItem.style.display = 'none';

    /**
     * Appends the 'invisible' file input inside the wrapper element.
     *
     * @param {Element} element
     *      Element wrapper, to append the file input to
     */
    $ctrl.appendItem = function(element) {
        element[0].firstChild.append($ctrl.inputItem);
    };

    /**
     * Shows image on the directive's view.
     *
     * @param {FileList} file
     *      File list with the current file
     */
    $ctrl.showImage = function(file) {
    };

    /**
     * Handles the 'dragover' event.
     *
     * @param {Event} evt
     *      Event object
     */
    $ctrl.elementOver = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        console.log('Is Over');

        // Set dragover class
        evt.target.classList.add('dragover');
    };

    /**
     * Handles the 'dragleave' event.
     *
     * @param {Event} evt
     *      Event object
     */
    $ctrl.elementExit = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        console.log('Exited');

        // Remove dragover class
        evt.target.classList.remove('dragover');
    };

    /**
     * Handles the 'drop' event.
     *
     * @param {Event} evt
     *      Event object
     */
    $ctrl.elementDrop = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        console.log('Dropped');

        // Adds the dropped class
        evt.target.classList.add('dropped');


    };

    /**
     * Handles the 'click' event.
     *
     * @param {Event} evt
     *      Event object
     */
    $ctrl.elementClick = function(evt) {
        evt.stopPropagation();
        $ctrl.inputItem.click();
    };

    /**
     * Handles the 'change' event.
     *
     * @param {Event} evt
     *      Event object
     */
    $ctrl.elementUpdate = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        // If files are present
        if (evt.target.files) {
            $scope.fileList = evt.target.files[0];
            // Check image type
            switch ($scope.fileList.type) {
                case 'image/gif':
                case 'image/jpg':
                case 'image/jpeg':
                case 'image/png':
                    // Apply changes
                    $scope.$apply();
                    break;
                default:
                    $ctrl.resetImageData();
                    break;
            }
        } else {
            $ctrl.resetImageData();
        }
    };

    /**
     * Reset image data.
     */
    $ctrl.resetImageData = function() {
        $ctrl.inputItem.value = '';
        $scope.imageData = '';
        $scope.fileList = null;
    };
}
