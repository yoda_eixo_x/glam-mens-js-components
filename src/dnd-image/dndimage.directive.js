dndimage.directive('dndImage', function() {
    return {
        scope: {
            fileList: '='
        },
        restrict: 'E',
        controller: DndImageInputController,
        link: function(scope, element, attrs, controller) {
            console.log(controller.aa);

            element[0].firstChild.appendChild(scope._input);
            element.on('dragover', scope._elDragOver);
            element.on('dragleave', scope._elDragLeave);
            element.on('drop', scope._elDragDrop);
            element.on('click', scope._elDragClick);
            element.on('change', scope._elDragChange);
        },
        templateUrl: 'dndimage.template.html'
    };
});

DndImageInputController.$inject = ['$scope'];

function DndImageInputController ($scope) {
    $scope._image = '';

    $scope._input = document.createElement('input');
    $scope._input.setAttribute('type', 'file');
    $scope._input.setAttribute('multiple', false);
    $scope._input.style.display = 'none';

    $scope._showImage = function(files) {
        var reads = new FileReader();
        reads.readAsDataURL(files[0]);
        reads.onload = function() {
            $scope._image = reads.result;
            $scope.$apply();
        };
        reads.onerror = function(error) {
            $scope._image = '';
            $scope.$apply();
            console.log(error);
        };
    };

    $scope._elDragOver = function(evt) {
        console.log(evt);
        evt.preventDefault();
        evt.stopPropagation();
        evt.target.classList.add('dragover');
    };

    $scope._elDragLeave = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        evt.target.classList.remove('dragover');
    };

    $scope._elDragDrop = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        evt.target.classList.remove('dragover');
        evt.target.classList.add('dropped');

        if (evt.dataTransfer) {
            $scope.fileList = evt.dataTransfer.files;
            $scope.$apply();
        } else if (evt.originalEvent.dataTransfer) {
            $scope.fileList = evt.originalEvent.dataTransfer.files;
            $scope.$apply();
        }

        if ($scope.fileList) {
            $scope._showImage($scope.fileList);
        }
    };

    $scope._elDragClick = function(evt) {
        evt.stopPropagation();
        $scope._input.click();
    };

    $scope._elDragChange = function(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        $scope.fileList = evt.target.files;
        $scope.$apply();

        if ($scope.fileList) {
            $scope._showImage($scope.fileList);
        }
    };
}
