lists.component("listsCards", {
    templateUrl: "lists-cards.template.html",
    bindings: {
        type: "<",
        parentStyle: "<",
        cards: "<",
        filter: "<",
    },
    controller: function ListsCardsController() {
    	var ctrl = this;

    	//Type default = image on left without subtitles, text on righ
        //Type leftsub = image on left with subtitles, text on righ
        //Type leftsubmore = button see more on the bottom of text part that redirects to another page
        //TODO Type top = image on top, text on bottom
        //TODO Type right = text on left, image with subtiles on right
        //TODO Type rightsub = text on left, image without subtitles on right
    	ctrl.$onInit = function() {
    		ctrl.type = ctrl.type === undefined ? "default" : ctrl.type;
            ctrl.filter = ctrl.filter === undefined ? "" : ctrl.filter;
    	};
    }
});