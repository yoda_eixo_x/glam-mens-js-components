lists.component("listsTiles", {
    templateUrl: "lists-tiles.template.html",
    bindings: {
    	onClickAction: "<",
        type: "<",
        tiles: "<",
        filter: "<",
        parentStyle: "<",
        onClickResponse: "&"
    },
    controller: function ListsTilesController() {
    	var ctrl = this;

    	//Type default = tile is only image
    	//Type imgsubtitle = tile image with sub
    	ctrl.$onInit = function() {
    		ctrl.type = ctrl.type === undefined ? "default" : ctrl.type;
    		ctrl.filter = ctrl.filter === undefined ? "" : ctrl.filter;
    		ctrl.onClickAction = ctrl.onClickAction === undefined ? "default" : ctrl.onClickAction;
    	};

    	ctrl.tileSelection = function(tile) {
    		if (ctrl.onClickAction === "default") {
    			ctrl.tiles.selected = tile;
    			ctrl.onClickResponse(ctrl.tiles);
    		} else if (ctrl.onClickAction === "goto") {
    			window.location.href = tile.image.goto;
    		}
    	};
    }
});