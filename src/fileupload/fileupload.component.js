/**
 * GLAM MENS JS COMPONENTS :: File Upload
 * ======================================================================
 * Componente para upload de arquivos, em estágio experimental.
 * ----------------------------------------------------------------------
 * @since       v0.0.1
 */
fileupload.component(
    'fileUpload',
    {
        bindings: {
            params: '<',
            upload: '<'
        },
        restrict: 'E',
        templateUrl: 'fileupload.template.html',
        controller: fileUploadController
    }
);

// Controller injection (nothing for now)
fileUploadController.$inject = [];

/**
 * File Upload Component Controller.
 */
function fileUploadController()
{
    /**
     * Object alias.
     *
     * @type {fileUploadController}
     */
    var ctrl = this;

    // SILENCE IS GOLDEN
    // ------------------------------------------------------------------
}