/**
 * GLAM MENS JS COMPONENTS :: Upload Item :: Service
 * ======================================================================
 * Service that handles file uploads through XMLHttpRequest, for the file
 * input directive.
 * ----------------------------------------------------------------------
 * @since       v0.0.1
 */
fileupload.service(
    'uploadItemService',
    function uploadItemService() {
        var upload = this;

        upload.uploadSend = function( scope, file, options )
        {
            this.file       = file;
            this.scope      = scope;
            this.options    = options;
            this.started    = false;
            this.progress   = 0;
        };

        upload.uploadSend.prototype.onProgress = function( evt )
        {
            this.progress = ( evt.lengthComputable ) ? Math.round( evt.loaded * 100 / evt.total ) : 0;
            if ( this.options.progress ) this.options.progress( this );
        };

        upload.uploadSend.prototype.onLoad = function( evt )
        {
            this.progress = 100;

            if ( evt.target.status == 200 ) {
                if ( this.options.success ) {
                    this.options.success( this, evt.target );
                }
            } else {
                if ( this.options.error ) {
                    this.options.error( this, evt.target );
                }
            }
        };

        upload.uploadSend.prototype.onError = function( evt )
        {
            this.progress = 100;

            if ( this.options.error ) {
                this.options.error( this, evt.target );
            }
        };

        upload.uploadSend.prototype.onAbort = function( evt )
        {
            if ( this.options.error ) {
                this.options.error( this, evt.target );
            }
        };

        upload.uploadSend.prototype.errorMessage = function( message )
        {
            var msgs        = ( message !== null && message !== '' ) ? message : 'Erro desconhecido.';
            this.xhr        = null;
            this.formdata   = null;
            this.progress   = 100;
            this.cssClass   = 'error';
            this.result     = {
                members:    [],
                message:    msgs,
                resultType: "FAILED",
                result:     null,
                exception:  null,
                start_data: 0,
                end_date:   0
            };
            this.label      = msgs;
        };

        upload.uploadSend.prototype.start = function()
        {
            if (
                this.options.maxsize > 0 && this.file.size > this.options.maxsize
            ) {
                var fileSize = ( this.file.size / 1024 ).toFixed( 2 ) + 'kb';
                var maxSize = ( this.options.maxsize / 1024 ).toFixed( 2 ) + 'kb';

                var msgs = this.file.name + ": o arquivo enviado excede o tamanho máximo de " + maxSize + " (Enviado: " + fileSize + ")";

                this.errorMessage( msgs );
                return false;
            }

            var self = this;
            this.xhr = new XMLHttpRequest();
            this.formdata = new FormData();
            this.formdata.append( 'uploadedFile', this.file );
            if ( this.options.data ) {
                var keys = Object.keys( this.options.data );
                for ( var i = 0; i < keys.length; $i++ ) {
                    this.formdata.append( keys[i], this.options.data[keys[i]] );
                }
            }
            this.xhr.upload.addEventListener( 'progress', function( e ) {
                self.onProgress( e );
            } );
            this.xhr.addEventListener( 'load', function( e ) {
                self.onLoad( e );
            } );
            this.xhr.addEventListener( 'error', function( e ) {
                self.onError( e );
            } );
            this.xhr.addEventListener( 'abort', function( e ) {
                self.onAbort( e );
            } );
            this.xhr.open(
                'POST',
                this.options.target
            );
            if ( this.options.headers ) {
                for ( var k in this.options.headers ) {
                    this.xhr.setRequestHeader( k.toString(), this.options.headers[k] );
                }
            }
            this.xhr.send( this.formdata );
            this.started = true;
        };
    }
);
