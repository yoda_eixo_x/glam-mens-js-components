/**
 * GLAM MENS JS COMPONENTS :: Upload Item :: Directive
 * ======================================================================
 * Directive that manages the file input used in the file uploader.
 * ----------------------------------------------------------------------
 * @since       v0.0.1
 */
fileupload.directive(
    'uploadItem',
    function() {
        return {
            controller: uploadItemController,
            link: function( scope, element, attrs ) {
                console.log(element);
                element.on( 'change', scope.itemUpload );

                // EXPERIMENTAL ---------- DO NOT TOUCH IF NOT DEVELOPING!
                /**
                 element.on( 'click', function( e ) {
                    e.stopPropagation();
                    // Input and Label handles
                    var input = element[0].children[0].firstElementChild;
                    var label = element[0].children[0];

                    input.click();
                });
                 */
                // EXPERIMENTAL ---------- DO NOT TOUCH IF NOT DEVELOPING!
            },
            restrict: 'E',
            scope: {
                params: '=',
                demoData: '='
            },
            templateUrl: 'uploaditem.template.html'
        };
    }
);

/**
 * GLAM MENS JS COMPONENTS :: Upload Item :: Directive
 * ======================================================================
 * Directive that manages the file input used in the file uploader.
 * ----------------------------------------------------------------------
 * @since       v0.0.1
 */

// Inject scope and service (scope is needed for updates)
uploadItemController.$inject = [ '$scope', 'uploadItemService' ];

/**
 * File input upload controller.
 *
 * @param {object} $scope
 *      Controller scope
 * @param {object} uploadItemService
 *      File upload service
 */
function uploadItemController( $scope, uploadItemService )
{
    /**
     * List containing the files being listed.
     *
     * @type {Array}
     */
    $scope.uploadFiles = [];

    /**
     * List containing the current files in the upload queue.
     *
     * @type {Array}
     */
    $scope.uploadQueue = [];

    /**
     * Updates upload item class on progress.
     *
     * @param {object} uploader
     *      Uploader object instance
     */
    var itemProgress = function( uploader )
    {
        uploader.cssClass = 'active';
        $scope.$apply();
    };

    /**
     * Executes if the upload was a success, but success means "file sent", you
     * still can have an upload error on the server side, so check the response
     * and results.
     *
     * If the result is !== null, pushes the response data into the uploadFiles,
     * and syncs with the scope's params array too.
     *
     * @param {object} uploader
     *      Uploader object instance
     * @param {object} response
     *      XMLHttpRequest server response
     *
     */
    var itemSuccess = function( uploader, response )
    {
        uploader.result = JSON.parse( response.responseText );
        uploader.label  = uploader.result.message || uploader.result.resultType;
        uploader.cssClass = ( uploader.result.resultType == 'SUCCESS' ) ? 'success' : 'error';

        console.log( uploader.result );

        if ( uploader.result.result !== null ) {
            console.log( 'NOT NULL' );
            $scope.uploadFiles.push( uploader.result.result );
        }
        $scope.params.finishedList = $scope.uploadFiles;
        $scope.$apply();
    };

    /**
     * Executes when an error happens while uploading the file.
     *
     * @param {object} uploader
     *      Uploader object instance
     * @param {object} response
     *      XMLHttpRequest server response
     */
    var itemError = function( uploader, response )
    {
        uploader.cssClass = 'error';
        try {
            var d = eval;
            uploader.result = d( response.responseText );
            uploader.label = uploader.result.message;
        } catch( e ) {
            //uploader.label = response.statusText;
            uploader.errorMessage(
                "Conexão abortada ou recusada."
            );
        }
        $scope.$apply();
    };

    /**
     * Item upload function.
     *
     * @param {event} evt
     *      Event handler
     */
    $scope.itemUpload = function( evt )
    {
        evt.stopPropagation();
        evt.preventDefault();

        if ( $scope.uploadFiles.length > 0 ) $scope.uploadFiles = [];
        if ( $scope.uploadQueue.length > 0 ) $scope.uploadQueue = [];

        var fileList = evt.target.files;
        for ( var i = 0; i < fileList.length; i++ ) {
            var upload = new uploadItemService.uploadSend(
                $scope,
                fileList[i],
                {
                    target: $scope.params.url,
                    progress: itemProgress,
                    success: itemSuccess,
                    error: itemError,
                    headers: $scope.params.headers,
                    maxsize: $scope.params.maxsize || 0
                }
            );
            $scope.uploadQueue.push( upload );
            upload.start();
        }
        $scope.params.uploadList = $scope.uploadQueue;
        $scope.$apply();
    };
}
