/**
 * GLAM MENS JS COMPONENTS :: File Upload
 * ======================================================================
 * Módulo File Upload
 * ----------------------------------------------------------------------
 * @since       v0.0.1
 */
var fileupload = angular.module(
    'fileupload',
    []
);
