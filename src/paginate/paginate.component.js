/**
 * GLAM MENS JS COMPONENTS :: Paginate
 * ======================================================================
 * Componente para paginação de dados.
 * ----------------------------------------------------------------------
 * @since       v0.0.1
 */
paginate.component(
    'paginateData',
    {
        bindings: {
            params: '<'
        },
        restrict: 'E',
        templateUrl: 'paginate.template.html',
        controller: PaginateController
    }
);

// Injections
PaginateController.$inject = [ '$location' ];

// Pagination Controller
function PaginateController( $location )
{
    /**
     * Object handle.
     *
     * @type {PaginateController}
     */
    var ctrl = this;

    /**
     * Runs when the component is started, checks for undeclared values and sets
     * default ones.
     */
    ctrl.$onInit = function()
    {
        // Default page size
        if ( !ctrl.params.page_size || ctrl.params.page_size <= 1 ) ctrl.params.page_size = 10;

        // Checks for default classes
        if ( !ctrl.params.wrapper_class ) ctrl.params.wrapper_class = 'btn-group';
        if ( !ctrl.params.default_class ) ctrl.params.default_class = 'btn btn-default';
        if ( !ctrl.params.current_class ) ctrl.params.current_class = 'btn btn-default active';

        // Toggle navigation
        if ( ctrl.params.show_arrows === undefined ) ctrl.params.show_arrows = true;
        if ( ctrl.params.show_numbers === undefined ) ctrl.params.show_numbers = true;

        // Default navigation numbers
        if ( !ctrl.params.number_side || ctrl.params.number_side <= 1 ) ctrl.params.number_side = 2;

        // Checks default First/Previous/Next/Last names
        if ( !ctrl.params.arrow_first_text ) ctrl.params.arrow_first_text = 'Primeira';
        if ( !ctrl.params.arrow_prev_text ) ctrl.params.arrow_prev_text = 'Anterior';
        if ( !ctrl.params.arrow_next_text ) ctrl.params.arrow_next_text = 'Próxima';
        if ( !ctrl.params.arrow_last_text ) ctrl.params.arrow_last_text = 'Última';

        ctrl.params.page_last = Math.ceil(
            ctrl.params.data_size / ctrl.params.page_size
        );

        ctrl.params.page_url = ctrl.params.page_url || false;
        if ( ctrl.params.page_url && ctrl.params.page_url.match( /(\/||\\)$/ )[0] === '' ) {
            ctrl.params.page_url += '/';
        }

        ctrl.params.current = parseInt( ctrl.params.current );
        ctrl.updateVals( ctrl.params.current );
        ctrl.updateNumbers();
    };

    /**
     * Checks when any variable suffers changes.
     */
    ctrl.$doCheck = function()
    {
        ctrl.params.page_last = Math.ceil(
            ctrl.params.data_size / ctrl.params.page_size
        );

        ctrl.params.current = parseInt( ctrl.params.current );
        ctrl.updateVals( ctrl.params.current );
        ctrl.updateNumbers();
    };

    /**
     * Moves pagination to the desired page.
     *
     * @param {string|integer} page
     *      Page number to travel to or one of the four "magic" words: first,
     *      prev, next and last, which takes you to "first, previous, next and
     *      last" pages, respectively
     */
    ctrl.goToPage = function( page )
    {
        if ( !isNaN( parseInt( page ) ) && parseInt( page ) !== 0 ) {
            ctrl.params.current = parseInt( page );
        } else {
            switch (page) {
                case 'first':
                    ctrl.params.current = 1;
                    break;
                case 'prev':
                    ctrl.params.current = (ctrl.params.current <= 1) ? 1 : ctrl.params.current - 1;
                    break;
                case 'next':
                    ctrl.params.current = (ctrl.params.current >= ctrl.params.page_last ) ? ctrl.params.page_last : ctrl.params.current + 1;
                    break;
                case 'last':
                    ctrl.params.current = ctrl.params.page_last;
                    break;
                default:
                    ctrl.params.current = 1;
                    break;
            }
        }

        if ( ctrl.params.page_url ) {
            $location.path( ctrl.params.page_url + ctrl.params.current );
        }

        if ( ctrl.params.current < 1 ) ctrl.params.current = 1;
        if ( ctrl.params.current > ctrl.params.page_last ) {
            ctrl.params.current = ctrl.params.page_last;
        }

        ctrl.params.current = parseInt( ctrl.params.current );
        ctrl.updateVals( ctrl.params.current );
        ctrl.updateNumbers();
    };

    /**
     * Updates pagination values.
     *
     * @param {integer} current
     */
    ctrl.updateVals = function( current )
    {
        ctrl.params.page_prev = ( ctrl.params.current > 1 ) ? ctrl.params.current - 1 : 1;
        ctrl.params.page_next = ( ctrl.params.current < ctrl.params.page_last ) ? ctrl.params.current + 1 : ctrl.params.page_last;
    };

    /**
     * Updates the numbered pagination array.
     */
    ctrl.updateNumbers = function()
    {
        var number_init = 1;
        var number_last = ctrl.params.page_last;
        var number_side = parseInt(ctrl.params.number_side);

        ctrl.params.number_list = [];

        if ( ctrl.params.page_last > 1 ) {
            if ( ctrl.params.page_last > 1 + ( number_side * 2 ) ) {
                if ( ctrl.params.current <= number_side ) {
                    number_init = 1;
                    number_last = 1 + ( number_side * 2 );
                } else if ( ctrl.params.current >= ctrl.params.page_last - number_side ) {
                    number_init = ctrl.params.page_last - ( number_side * 2 );
                    number_last = ctrl.params.page_last;
                } else {
                    number_init = ctrl.params.current - number_side;
                    number_last = ctrl.params.current + number_side;
                }
            } else {
                number_init = 1;
                number_last = ctrl.params.page_last;
            }

            for ( var n = number_init; n <= number_last; n++ ) {
                ctrl.params.number_list.push( n );
            }
        }
    };
}