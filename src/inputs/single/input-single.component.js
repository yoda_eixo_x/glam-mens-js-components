inputs.component("inputSingle", {
    templateUrl: "input-single.template.html",
    bindings: {
        input: "<",
        label: "<",
        labelInputStyle: "<",
        validation: "<",
        onUpdate: "&"
    },
    controller: function InputSingleController() {
        var ctrl = this;
        ctrl.$onInit = function() {
            ctrl.label.type = ctrl.label === undefined ? "simple" : ctrl.label.type;
            ctrl.input.type = ctrl.input.type === undefined ? "text" : ctrl.input.type;
            ctrl.input.rows = ctrl.input.rows === undefined ? 4 : ctrl.input.rows;
            ctrl.input.cols = ctrl.input.cols === undefined ? 50 : ctrl.input.cols;
        };
        ctrl.$doCheck = function() {
            ctrl.onUpdate(ctrl.input);
        };
    }
});