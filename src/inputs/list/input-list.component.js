inputs.component("inputList", {
    templateUrl: "input-list.template.html",
    bindings: {
        input: "<",
        label: "<",
        labelInputStyle: "<",
        validation: "<",
        onUpdate: "&"
    },
    controller: function InputListController() {
        var ctrl = this;
        ctrl.checkboxes = [];
        ctrl.$onInit = function() {
            ctrl.label.type = ctrl.label.type === undefined ? "simple" : ctrl.label.type;
            ctrl.input.type = ctrl.input.type === undefined ? "text" : ctrl.input.type;
            ctrl.input.optionsType = ctrl.input.optionsType === undefined ? "value" : ctrl.input.optionsType;
            if (ctrl.input.type === "checkbox") {
                for (var i = 0; i < ctrl.input.options.length; i++) {
                    var checkbox = {
                        name: ctrl.input.name + "-" + i,
                        value: ctrl.input.options[i],
                        response: false
                    };
                    ctrl.checkboxes.push(checkbox);
                }
                ctrl.input.value = [];
                for (i = 0; i < ctrl.checkboxes.length; i++) {
                    ctrl.input.value[i] = ctrl.checkboxes[i].response;
                }
            }
        };
        ctrl.$doCheck = function() {
            for (i = 0; i < ctrl.checkboxes.length; i++) {
                ctrl.input.value[i] = ctrl.checkboxes[i].response;
            }
            ctrl.onUpdate(ctrl.input);
        };
    }
});