# Glambox / Men's Market Javascript Components - Inputs Components #

## What are these components for? ##
Create input's or group of inputs on your webapp

### Single Input ###
Create a single input with open value, and returns the value typed. In case of a checkbox, returns true of false if the value was selected or not.

#### Component Input ####
Object - Input
Object - Label
String - labelInputStyle

#### Component Output ####
Object - onUpdate


### List Input ###
Create an that may be populated by a list given by the caller

#### Component Input ####
Object - Input
Object - Label
String - labelInputStyle

#### Component Output ####
Object - onUpdate

### Arrange Inputs ###

