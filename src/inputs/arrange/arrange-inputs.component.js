inputs.component("arrangeInputs", {
    templateUrl: "arrange-inputs.template.html",
    bindings: {
        divStyle: "<",
        inputsLabels: "<",
        onUpdate: "&"
    },
    controller: function ArrangeInputsController() {
        var ctrl = this;
        ctrl.$onInit = function() {
            ctrl.blocked = false;
            var blocks = [];
            ctrl.repetitions = [];
            if (ctrl.divStyle.includes("row")) {
                var rowSize = 0;
                for (var i = 0; i < ctrl.inputsLabels.length; i++) {
                    rowSize += ctrl.inputsLabels[i].gridColSize;
                    ctrl.inputsLabels[i].labelInputStyle += ctrl.inputsLabels[i].gridColSize;
                    blocks.push(ctrl.inputsLabels[i]);
                    if (rowSize >= 12) {
                        ctrl.repetitions.push(blocks);
                        ctrl.blocked = true;
                        rowSize = 0;
                        blocks = [];
                    }
                }
            }
        };
    }
});