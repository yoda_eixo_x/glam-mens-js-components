helper.factory("HelperFactory", function() {
    return {
        getTrigger: function(value) {
            if (value.trigger === "hover") {
                return "mouseenter";
            } else if (value.trigger === "focus") {
                return "focusin";
            }
            return value.trigger;
        },
        getOptions: function(value) {
            return {
                content: value.message,
                placement: value.position,
                title: value.title,
                trigger: value.trigger,
            };
        }
    };
});