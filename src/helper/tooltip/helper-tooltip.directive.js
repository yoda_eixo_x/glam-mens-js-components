helper.directive("tooltip", ["HelperFactory", function(HelperFactory) {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            scope.$watch(attrs.tooltip, function(value) {
                if (value.trigger !== undefined) {
                    var elemTrigger = HelperFactory.getTrigger(value);
                    elem.on(elemTrigger, function() {
                        var options = HelperFactory.getOptions(value);
                        elem = elem.find(value.type);
                        elem.tooltip(options);
                        elem.tooltip("show");
                    });
                }
            });
        }
    };
}]);