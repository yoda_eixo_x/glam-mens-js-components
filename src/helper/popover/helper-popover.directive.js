helper.directive("popover", ["HelperFactory", function(HelperFactory) {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            scope.$watch(attrs.popover, function(value) {
                if (value.trigger !== undefined) {
                	var elemTrigger = HelperFactory.getTrigger(value);
                    elem.on(elemTrigger, function() {
                    	var options = HelperFactory.getOptions(value);
                    	elem = elem.find(value.type);
                    	elem.popover(options);
                        elem.popover("show");
                    });
                }
            });
        }
    };
}]);

