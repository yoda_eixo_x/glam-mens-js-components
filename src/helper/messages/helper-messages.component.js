helper.component("helperMessages", {
    templateUrl: "helper-messages.template.html",
    bindings: {
        messages: "<"
    }
});