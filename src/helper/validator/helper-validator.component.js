helper.component("helperValidator", {
    templateUrl: "helper-validator.template.html",
    bindings: {
        value: "<",
        validation: "<",
    },
    controller: function HelperValidatorController() {
        var ctrl = this;
        ctrl.$doCheck = function() {
            var failRequ = false;
            var failMin = false;
            var failMax = false;
            if (ctrl.validation.required && ctrl.value === undefined || ctrl.value === "") {
                ctrl.message = "Campo acima é obrigatório.";
                ctrl.success = false;
                failRequ = true;
            } else if (ctrl.value !== undefined && ctrl.value.length < ctrl.validation.min) {
                ctrl.message = "Cadastre um valor com pelo menos " + ctrl.validation.min + " carácteres no campo acima.";
                ctrl.success = false;
                failMin = true;
            } else if (ctrl.value !== undefined && ctrl.value.length > ctrl.validation.max) {
                ctrl.message = "Cadastre um valor com menos de " + ctrl.validation.max + " carácteres no campo acima.";
                ctrl.success = false;
                failMax = true;
            }
            if (!failRequ && !failMin && !failMax) {
                ctrl.success = true;
                ctrl.message = "Uhuuuullllll";
            }
            // else if (scope.validRegex !== undefined) {
            //     //TODO
            // }
        };
    }
});