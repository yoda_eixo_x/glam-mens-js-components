/**
 * GLAM MENS JS COMPONENTS :: DND Upload Directive Controller
 * ======================================================================
 * 
 * ----------------------------------------------------------------------
 * @copyright ©2017 Glambox/Men's Market
 * @since 0.0.1
 */
dndupload.directive('dndUpload', function() {
    return {
        scope: {
            dndUpload: '='
        }, 
        restrict: 'A', 
        controller: DndUploadDirectiveController, 
        controllerAs: 'ctrl', 
        link: function (scope, element, attrs, ctrl) {
            ctrl.appendInput(element[0]);
            element.on('dragover', ctrl.eventOver);
            element.on('dragleave', ctrl.eventExit);
            element.on('drop', ctrl.eventDrop);
            element.on('change', ctrl.eventChange);
            element.on('click', ctrl.eventDown);
        }, 
        template: '<img ng-if="imageData !== \'\'" src="{{imageData}}" alt="">'
    };
});

// Dependency injection
DndUploadDirectiveController.$inject = ['$scope'];

// DndUpload Directive Controller
function DndUploadDirectiveController($scope) {
    var $ctrl = this;
    
    $scope.imageData = '';
    
    $ctrl.inputItem = document.createElement('input');
    $ctrl.inputItem.setAttribute('type', 'file');
    $ctrl.inputItem.setAttribute('multiple', false);
    $ctrl.inputItem.style.display = 'none';
    
    $ctrl.appendInput = function(element) {
        element.append($ctrl.inputItem);
    };
    
    $ctrl.eventOver = function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.target.classList.add('over');
    };
    
    $ctrl.eventExit = function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.target.classList.remove('over');
    };
    
    $ctrl.eventDrop = function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.target.classList.add('dropped');
        
        if (evt.dataTransfer) {
            $ctrl.pushImage(evt.dataTransfer.files);
        } else if (evt.originalEvent.dataTransfer) {
            $ctrl.pushImage(evt.originalEvent.dataTransfer.files);
        }
    };
    
    $ctrl.eventDown = function(evt) {
        evt.stopPropagation();
        $ctrl.inputItem.click();
        evt.target.classList.add('drop');
    };
    
    $ctrl.eventChange = function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        
        // If files are present
        if (evt.target.files) {
            $ctrl.pushImage(evt.target.files);
            evt.target.classList.add('dropped');
        } else {
            evt.target.classList.remove('dropped');
            $ctrl.resetImageData();
        }
    };
    
    $ctrl.pushImage = function(file) {
        // Check image type
        switch (file[0].type) {
            case 'image/gif':
            case 'image/jpg':
            case 'image/jpeg':
            case 'image/png':
                // Define file
                $scope.dndUpload = file;
                // Apply changes
                $scope.$apply();
                $ctrl.showImage();
                break;
            default:
                $ctrl.resetImageData();
                $ctrl.showImage();
                break;
        }
    };
    
    $ctrl.showImage = function() {
        if ($scope.dndUpload !== null) {
            var reads = new FileReader();
            reads.readAsDataURL($scope.dndUpload[0]);
            reads.onload = function() {
                $scope.imageData = reads.result;
                $scope.$apply();
            };
            reads.onerror = function(error) {
                $scope.imageData = '';
                $scope.$apply();
            };
        } else {
            $scope.imageData = '';
            $scope.$apply();
        }
    };

    /**
     * Reset image data.
     */
    $ctrl.resetImageData = function() {
        $ctrl.inputItem.value = '';
        $scope.imageData = '';
        $scope.dndUpload = null;
    };
}