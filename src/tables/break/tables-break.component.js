tables.component("tablesBreak", {
    templateUrl: "tables-break.template.html",
    bindings: {
        style: "<",
        header: "<",
        keyValues: "<",
        widthBreak: "<"
    },
    controller: TablesBreakController
});
TablesBreakController.$inject = ["$scope"];

function TablesBreakController($scope) {
    var ctrl = this;
    ctrl.$onInit = function() {
        //data
        var body = [];
        for (i = 0; i < ctrl.header.length; i++) {
            for (var j = 0; j < ctrl.keyValues.length; j++) {
                if (ctrl.header[i] === ctrl.keyValues[j].key) {
                    var elem = [];
                    for (var k = 0; k < ctrl.keyValues[j].values.length; k++) {
                        var obj = {
                            key: ctrl.keyValues[j].key,
                            value: ctrl.keyValues[j].values[k]
                        };
                        elem.push(obj);
                    }
                    body.push(elem);
                }
            }
        }
        ctrl.body = [];
        ctrl.body = body[0].map(function(col, i) {
            return body.map(function(row) {
                return row[i];
            });
        });
        //size
        ctrl.size = "normal";
        if (ctrl.widthBreak !== undefined) {
            var winWidth = window.innerWidth;
            if (winWidth <= ctrl.widthBreak) {
                ctrl.size = "small";
            }
        }
    };
    angular.element(window).bind("resize", function() {
        if (ctrl.widthBreak !== undefined) {
            var winWidth = window.innerWidth;
            if (window.innerWidth <= ctrl.widthBreak) {
                ctrl.size = "small";
                $scope.$digest();
            } else {
                ctrl.size = "normal";
                $scope.$digest();
            }
        }
    });
}