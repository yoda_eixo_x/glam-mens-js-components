# Glambox / Men's Market Javascript Components - Button Components #

## What are these components for? ##
* Create buttons with some general javascript behaviour on your web app.
* Button components are separated between submit, cancel and submit-cancel buttons.

### Cancel Button ###
* Default behaviour: go back to previous page

#### Options ####
* You can choose the button name and class of style

### Submit Button ###
* Default behaviour: response to caller with status and result of a RESTAPI requisition
* Custom Behaviour 1: In Case of success go to a page defined by the caller
* Custom Behaviour 2: In Case of success go back to previous page

#### Options ####
* You can choose the button name, the behaviour and class of style

### Submit-Cancel Set ###
* Creates a set of submit and cancel button, aligned on the right
* They behave according to the isolated buttons
