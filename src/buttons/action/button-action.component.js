buttons.component(
    'buttonAction',
    {
        templateUrl: 'button-action.template.html',
        bindings: {
            text: '<',
            action: '<',
            buttonStyle: '<'
        },
        controller: function ButtonActionController() {
            var ctrl = this;

            /**
             * Initializes all default parameters, when something is not set.
             */
            ctrl.$onInit = function() {
                ctrl.text = ctrl.text || 'Voltar';
                ctrl.action = ctrl.action || { name: 'default', execute: '', params: null };
                ctrl.buttonStyle = ctrl.buttonStyle || 'btn btn-default';
            };

            /**
             * Default action for the button, if no method was declared as a
             * parameter.
             *
             * Uses the 'action.name' parameter to perform some of the standard
             * actions.
             */
            ctrl.performAction = function()
            {
                var name = ctrl.action.name;
                var exec = ctrl.action.execute;

                if ( name === 'default' ) {
                    window.history.back();
                } else if ( name === 'goto' ) {
                    if ( exec && typeof( exec ) == 'string' ) {
                        window.location.href = exec;
                    }
                } else if ( name == 'modal' || name == 'clear' ) {
                    if ( exec ) exec( ctrl.action.params );
                } else {
                    console.log( 'No action assigned.' );
                }
            };
        }
    }
);