/**
 * GLAM MENS JS COMPONENTS : Buttons/Action
 * ======================================================================
 * Button Component, Action Type.
 * ----------------------------------------------------------------------
 * @since v0.0.1
 */
buttons.component(
    'buttonSubmit',
    {
        templateUrl: 'button-action.template.html',
        bindings: {
            text: '<',
            action: '<'
        },
        controller: ButtonActionController
    }
);

// Controller Injector
ButtonActionController.$inject = ['$location'];

// Button Action Controller
function ButtonActionController($location) {
    /**
     * Controller alias.
     */
    var ctrl = this;

    /**
     * Controller initialization code.
     */
    ctrl.$onInit = function() {
        // Check for default values on bindings
        ctrl.text = ctrl.text || 'Voltar';
        ctrl.action = ctrl.action || {name: 'default', execute: '', params: null};
        ctrl.buttonStyle = ctrl.buttonStyle || 'btn btn-default';
    };

    /**
     * Executes the default actions or whatever action the user has set for
     * this button.
     *
     * What defines which action will be performed is the 'action.name' parameter.
     */
    ctrl.performAction = function() {
        var name = ctrl.action.name;
        var exec = ctrl.action.execute;

        if (name === 'default') {
            window.history.back();
        } else if (name === 'goto') {
            if (exec && typeof(exec) === 'string') $location.path(exec);
        } else if (name === 'modal' || name === 'clear') {
            if (exec && typeof(exec) === 'function') exec(ctrl.action.params);
        } else {
            console.log( 'No action assigned.' );
        }
    };
}