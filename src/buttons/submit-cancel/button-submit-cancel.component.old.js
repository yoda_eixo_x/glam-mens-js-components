/**
 * GLAM MENS JS COMPONENTS : Buttons/Submit-Cancel
 * ======================================================================
 * Button component, grouped Submit and Cancel buttons.
 * ----------------------------------------------------------------------
 * @since v0.0.1
 */
buttons.component(
    'buttonsSubmitCancel',
    {
        bindings: {
            cancel: '<',
            submit: '<',
            buttonGroupStyle: '<',
            buttonSubmitStyle: '<',
            buttonCancelStyle: '<',
            onSubmit: '&'
        },
        templateUrl: 'button-submit-cancel.template.html',
        controller: ButtonSubmitCancelController
    }
);

// Controller Injector
ButtonSubmitCancelController.$inject = ['$http', '$location'];

// Button Submit/Cancel Controller
function ButtonSubmitCancelController($http, $location) {
    /**
     * Controller alias.
     */
    var ctrl = this;

    /**
     * Controller initialization code.
     */
    ctrl.$onInit = function() {
        // Check for default values on bindings
        ctrl.cancel = ctrl.cancel || {};
        ctrl.submit = ctrl.submit || {};
        ctrl.buttonGroupStyle = ctrl.buttonGroupStyle || '';
        ctrl.buttonSubmitStyle = ctrl.buttonSubmitStyle || '';
        ctrl.buttonCancelStyle = ctrl.buttonCancelStyle || '';
        ctrl.onSubmit = ctrl.onSubmit || '';
    };

    ctrl.onButtonSubmit = function(params) {
    };
}