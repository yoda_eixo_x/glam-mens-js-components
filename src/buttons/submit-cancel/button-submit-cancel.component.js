buttons.component("buttonsSubmitCancel", {
    templateUrl: "button-submit-cancel.template.html",
    bindings: {
        cancel: "<",
        submit: "<",
        buttonsGroupStyle: "<",
        onSubmit: "&",
    },
    controller: function ButtonsSubmitCancelController() {
        var ctrl = this;
        ctrl.$onInit = function() {
            //overrides definition in the object of individual button
            //options: btn-group-lg, btn-group-sm, btn-group-xs. Default: none
            if (ctrl.buttonsGroupStyle !== undefined) {
                if (ctrl.cancel === undefined && ctrl.submit === undefined) {
                    ctrl.cancel = {};
                    ctrl.submit = {};
                }
                if (ctrl.cancel.buttonStyle === undefined) {
                    ctrl.cancel.buttonStyle = " ";
                }
                if (ctrl.submit.buttonStyle === undefined) {
                    ctrl.submit.buttonStyle = " ";
                }
                ctrl.cancel.buttonStyle = ctrl.cancel.buttonStyle + " " + ctrl.buttonsGroupStyle.replace("-group-", "-");
                ctrl.submit.buttonStyle = ctrl.submit.buttonStyle + " " + ctrl.buttonsGroupStyle.replace("-group-", "-");
            }
        };
        ctrl.onButtonSubmit = function(params) {
            ctrl.submit.params.success = params.success;
            ctrl.submit.params.result = params.result;
            ctrl.onSubmit(ctrl.submit);
        };
        ctrl.onButtonSubmit = function(params) {
            ctrl.submit.params.success = params.success;
            ctrl.submit.params.result = params.result;
            ctrl.onSubmit(ctrl.submit);
        };
    }
});