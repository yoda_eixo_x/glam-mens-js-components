/**
 * GLAM MENS JS COMPONENTS : Buttons/Submit
 * ======================================================================
 * Button Component, Submit Type.
 * ----------------------------------------------------------------------
 * @since v0.0.1
 */
buttons.component(
    'buttonSubmit',
    {
        bindings: {
            text: '<',
            mode: '<',
            goTo: '<',
            params: '<',
            buttonStyle: '<',
            onSubmit: '&'
        },
        templateUrl: 'button-submit.template.html',
        controller: ButtonSubmitController
    }
);

// Controller Injector
ButtonSubmitController.$inject = ['$http', '$location'];

// Button Submit Controller
function ButtonSubmitController($http, $location) {
    /**
     * Controller alias.
     */
    var ctrl = this;

    /**
     * Controller initialization code.
     */
    ctrl.$onInit = function() {
        // Check for default values on bindings
        ctrl.text = ctrl.text || 'Gravar';
        ctrl.mode = ctrl.mode || 'default';
        ctrl.goTo = ctrl.goTo || '';
        ctrl.params = ctrl.params || {};
        ctrl.buttonStyle = ctrl.buttonStyle || 'btn btn-primary';
        ctrl.onSubmit = ctrl.onSubmit || false;
    };

    /**
     * Sends the HTTP request.
     */
    ctrl.submitData = function() {
        $http({
            url: ctrl.params.url,
            method: 'POST',
            data: ctrl.params.data,
            headers: ctrl.params.headers
        }).then(onSuccess, onFailure);
    };

    /**
     * Callback for a successful request, when submitting data.
     *
     * @param {object} response
     */
    ctrl.onSuccess = function(response) {
        if (ctrl.mode === 'default') {
            ctrl.params.success = true;
            ctrl.params.result = response.data.result;
            ctrl.params.message = response.data.message;
            ctrl.onSubmit(ctrl.params);
        } else if (ctrl.mode === 'goto') {
            if (ctrl.goTo) $location.path(ctrl.goTo);
        } else if (ctrl.mode === 'goback') {
            window.history.back();
        } else {
            console.log('Wrong mode for this button type.');
        }
    };

    /**
     * Callback for an unsuccessful request, when submitting data.
     */
    ctrl.onFailure = function() {
        ctrl.params.success = false;
        ctrl.onSubmit(ctrl.params);

        // TODO: Specific modes for failure
        if (ctrl.mode === 'default') {
        } else {
        }
    };
}