buttons.component("buttonSubmit", {
    templateUrl: "button-submit.template.html",
    bindings: {
        text: "<",
        mode: "<",
        goTo: "<",
        params: "<",
        buttonStyle: "<",
        onSubmit: "&"
    },
    controller: ButtonSubmitController
});
ButtonSubmitController.$inject = ["$http"];

function ButtonSubmitController($http) {
    var ctrl = this;
    ctrl.$onInit = function() {
        ctrl.text = ctrl.text === undefined ? "Gravar" : ctrl.text;
        ctrl.mode = ctrl.mode === undefined ? "default" : ctrl.mode;
        //options: btn-default, btn-primary, btn-success, btn-info, btn-warning, btn-danger and combine with size: btn-default btn-sm. Default: btn-primary
        ctrl.buttonStyle = ctrl.buttonStyle === undefined ? "btn-primary" : ctrl.buttonStyle;
    };
    ctrl.submit = function() {
        $http({
            url: ctrl.params.url,
            method: 'POST',
            data: ctrl.params.data,
            headers: ctrl.params.headers
        }).then(function onSuccess(response) {
            if (
                response.data.resultType && response.data.resultType === "SUCCESS"
                || response.data.result_type && response.data.result_type === "SUCCESS"

            ) {
                if (ctrl.mode === "default") {
                    ctrl.params.success = true;
                    ctrl.params.result = response.data.result;
                    ctrl.params.message = response.data.message;
                    ctrl.onSubmit(ctrl.params);
                } else if (ctrl.mode === "goto") {
                    window.location.href = ctrl.goTo;
                } else if (ctrl.mode === "goback") {
                    window.history.back();
                }
            } else {
                ctrl.params.success = false;
                ctrl.params.message = response.data.message;
                ctrl.onSubmit(ctrl.params);
            }
        }, function onFailure() {
            ctrl.params.success = false;
            ctrl.onSubmit(ctrl.params);
            //TODO specific modes for failure
            if (ctrl.mode === "default") {} else {}
        });
    };
}