modals.component(
    'modalDialog',
    {
        bindings: {
            params: '<'
        },
        restrict: 'E',
        templateUrl: 'modals.template.html',
        transclude: {
            body: 'modalBody',
            footer: 'modalFooter'
        },
        controller: ModalDialogController
    }
);

// Injections
ModalDialogController.$inject = [ '$timeout' ];

function ModalDialogController( $timeout )
{
    var ctrl = this;

    console.log( ctrl );

    ctrl.$onInit = function()
    {
        ctrl.current = ctrl.params.toggle;
        ctrl.display();
    };

    ctrl.$doCheck = function()
    {
        if ( ctrl.current !== ctrl.params.toggle ) {
            ctrl.current = ctrl.params.toggle;
            ctrl.display();
        }
    };

    ctrl.display = function()
    {
        ctrl.modal_fade = ( ctrl.params.toggle ) ? ' in' : '';

        if (ctrl.params.toggle === true) {
            ctrl.modal_zindex = 'overflow-y:auto !important';
        } else {
            $timeout(function () {
                ctrl.modal_zindex = 'z-index:-1;opacity:0;';
            }, 300);
        }
    };

    ctrl.closeModal = function()
    {
        ctrl.params.toggle = false;
    };
}