var app = angular.module("app", ["ngRoute", "ngGlamMensComponents"]);
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
    .when('/usecases/buttons', {
        templateUrl: 'buttons.html'
    }).when('/usecases/inputs', {
    	templateUrl: 'inputs.html'
    }).when('/usecases/helpers', {
    	templateUrl: 'helpers.html'
    }).when('/usecases/lists', {
        templateUrl: 'lists.html'
    }).when('/usecases/tables', {
        templateUrl: 'tables.html'
    }).when('/usecases/fileupload', {
        templateUrl: 'fileupload.html'
    }).when('/usecases/paginate', {
        templateUrl: 'paginatetest.html'
    }).when('/usecases/paginate/:page', {
        templateUrl: 'paginatetest.html'
    }).when('/usecases/panels', {
        templateUrl: 'panels.html'
    }).when('/usecases/modals', {
        templateUrl: 'modals.html'
    }).when('/usecases/dndimage', {
        templateUrl: 'dndimage.html'
    });
    /*
     .when('/notbasic/notbasic1', {
     templateUrl: 'notbasic-1.html'
     })
     */

}]);
