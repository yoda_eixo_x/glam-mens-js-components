app.controller('HelpersController', ['$scope', function($scope) {
    //Common objects used during the use cases
    $scope.label = {
        type: "simple",
        simple: {
            name: "label teste"
        },
        grouped: {
            style: "input-group-lg",
            left: {
                show: true,
                name: "escreve ae"
            },
            right: {
                show: true,
                name: "agora sim"
            }
        }
    };
    var options = [];
    options.push("teste 1");
    options.push("teste 2");
    options.push("teste 3");
    options.push("teste 4");
    $scope.input = {
        type: "text",
        name: "teste",
        placeholder: "placeholder",
        options: options
    };
    $scope.labelInputStyle = {};
    $scope.divStyle = "row";
    $scope.inputCase1 = angular.copy($scope.input);
    $scope.labelCase1 = angular.copy($scope.label);
    $scope.inputCase2 = angular.copy($scope.input);
    $scope.labelCase2 = angular.copy($scope.label);
    $scope.inputCase2.type = "radio";
    $scope.inputCase3 = angular.copy($scope.input);
    $scope.labelCase3 = angular.copy($scope.label);
    $scope.inputCase4 = angular.copy($scope.input);
    $scope.labelCase4 = angular.copy($scope.label);
    $scope.inputCase5 = angular.copy($scope.input);
    $scope.labelCase5 = angular.copy($scope.label);
    $scope.inputCase6 = angular.copy($scope.input);
    $scope.labelCase6 = angular.copy($scope.label);
    //Use Case Validation 1 - Single Input
    $scope.inputValid1 = {
        exec: true,
        style: "",
        error: {
            style: "erro-msg"
        },
        success: {
            style: "success-msg"
        },
        required: true,
        max: 10,
        min: 3
    };
    //Use Case Validation 2 - List Input
    $scope.inputValid2 = {
        exec: true,
        style: "",
        error: {
            style: "erro-msg"
        },
        success: {
            style: "success-msg"
        },
        required: true
    };
    //Use Case Popover 1 - onClick on the right side of the element
    $scope.popover1 = {
        type: "input",
        trigger: "click",
        title: "pop",
        message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at sapien lacinia, aliquet mauris ut, fringilla nulla. Donec elementum mauris sed est placerat, et vehicula tortor dapibus. Sed facilisis tristique tellus, sit amet consequat purus consequat vel. Nullam lobortis massa neque, in facilisis felis vestibulum at. In molestie arcu at orci luctus sollicitudin. Proin et ante nec nisi porttitor suscipit. Morbi tempus pharetra purus quis hendrerit. Mauris eleifend rhoncus enim sed maximus. Duis elementum iaculis fringilla. Vestibulum ultrices libero ac lectus sollicitudin, suscipit laoreet nibh feugiat. Nunc lacinia quam eros, sed luctus magna congue ut. Vestibulum dictum posuere diam, eget tempus libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et mattis diam. Suspendisse faucibus, lectus vitae sollicitudin bibendum, ex dui cursus sapien, ut lobortis risus sem ut nisl.",
        position: "right"
    };
    //Use Case Popover 2 - onHover on top of the element
    $scope.popover2 = {
        type: "input",
        trigger: "hover",
        title: "pop",
        message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at sapien lacinia, aliquet mauris ut, fringilla nulla. Donec elementum mauris sed est placerat, et vehicula tortor dapibus. Sed facilisis tristique tellus, sit amet consequat purus consequat vel. Nullam lobortis massa neque, in facilisis felis vestibulum at. In molestie arcu at orci luctus sollicitudin. Proin et ante nec nisi porttitor suscipit. Morbi tempus pharetra purus quis hendrerit. Mauris eleifend rhoncus enim sed maximus. Duis elementum iaculis fringilla. Vestibulum ultrices libero ac lectus sollicitudin, suscipit laoreet nibh feugiat. Nunc lacinia quam eros, sed luctus magna congue ut. Vestibulum dictum posuere diam, eget tempus libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et mattis diam. Suspendisse faucibus, lectus vitae sollicitudin bibendum, ex dui cursus sapien, ut lobortis risus sem ut nisl.",
        position: "top"
    };
    //Use Case Popover 3 - onFocus on bottom of the element
    $scope.popover3 = {
        type: "input",
        trigger: "focus",
        title: "pop",
        message: "teste popover",
        position: "bottom"
    };
    //Use Case Tooltip 1 - onClick on top of the element - there's a difference between the object of tooltip and popover
    $scope.tooltip = {
        type: "input",
        trigger: "click",
        title: "tip",
        position: "top"
    };
    //Use Case Messages 1 - Error
    $scope.messages1 = {
        show: true,
        style: "alert-danger alert-dismissible",
        text: "Não foi possivel realizar operação"
    };
    //Use Case Messages 1 - Success
    $scope.messages2 = {
        show: true,
        style: "alert-success alert-dismissible",
        text: "Operação realizada com sucesso"
    };
}]);