app.controller('ButtonsController', ['$scope', function($scope) {
    //Pre-requisites: local environment of APIs
    //Common objects used in tests
    var headers = {
        'Authorization': 'a6b3d8b6-1b25-4809-88fc-df1c5e9b1828'
    };
    var data = {
        page_size: 100
    };

    $scope.test_action = function() 
    {
        console.log( 'Test Action' );
    };

    //Use Case Action 1 - Go Back to previous location
    $scope.action1 = {
        text: 'Go Back, NOW!',
        buttonStyle: 'btn btn-default'
    };

    //Use Case Action 2 - Go To URL
    $scope.action2 = {
        text: 'Go To URL',
        action: {
            name: 'goto',
            execute: 'http://google.com'
        },
        buttonStyle: 'btn btn-info'
    };

    //Use Case Action 3 - Modal/Clear form/Custom action
    $scope.action3 = {
        text: 'Check the Console!!!',
        action: {
            name: 'modal',
            execute: $scope.test_action
        },
        buttonStyle: 'btn btn-warning'
    };

    $scope.params = {
        url: "http://localhost:8080/com.eixox.api/brand/All",
        data: data,
        headers: headers
    };

    //Use Case Submit 1 - Submit, check for response and return data
    $scope.submit1 = {
        text: "Submit + Return",
        buttonStyle: "btn-primary",
        mode: "default"
    };

    $scope.onSubmit = function(params) {
        console.log(params);
    };

    //Use Case Submit 2 - Submit, check for response and in case of success go to some page
    $scope.submit2 = {
        text: "Submit + Redirect",
        buttonStyle: "btn-warning",
        mode: "goto",
        goTo: "http://localhost:9090/index.html"
    };

    //Use Case Submit 3 - Submit, check for response and in case of success go to previous location
    $scope.submit3 = {
        text: "Submit + Return",
        buttonStyle: "btn-danger",
        mode: "goback"
    };

    //Use Case Submit and Cancel 1 - basic case of set of the two buttons
    $scope.groupCancel = {
        text: "Return",
    };

    $scope.groupSubmit = {
        text: "Submit",
        params: $scope.params
    };

    $scope.onSubmitGroup = function(submit) {
        console.log(submit.params);
    };
}]);