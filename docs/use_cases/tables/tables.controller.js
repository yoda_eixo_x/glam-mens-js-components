app.controller('TablesController', ['$scope', function($scope) {
    // Data used in both use cases
    $scope.header = [];
    $scope.header.push("CNPJ");
    $scope.header.push("Série");
    $scope.header.push("Número");
    $scope.header.push("Natureza");
    $scope.header.push("Data Emissão");
    $scope.header.push("Valor da Nota");
    var cnpjs = [];
    var series = [];
    var numeros = [];
    var naturezas = [];
    var datas = [];
    var valores = [];
    for (var i = 0; i < 5; i++) {
        cnpjs.push("CNPJ " + i);
        series.push("Série " + i);
        numeros.push("Número " + i);
        naturezas.push("Natureza " + i);
        datas.push("Data Emissão " + i);
        valores.push("Valor da Nota " + i);
    }
    $scope.keyValues = [];
    $scope.keyValues.push({
        key: "CNPJ",
        values: cnpjs
    });
    $scope.keyValues.push({
        key: "Série",
        values: series
    });
    $scope.keyValues.push({
        key: "Número",
        values: numeros
    });
    $scope.keyValues.push({
        key: "Natureza",
        values: naturezas
    });
    $scope.keyValues.push({
        key: "Data Emissão",
        values: datas
    });
    $scope.keyValues.push({
        key: "Valor da Nota",
        values: valores
    });

    // use only in use case break table
    $scope.break = 768;
}]);