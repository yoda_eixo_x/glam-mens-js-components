app.controller('InputsController', ['$scope', function($scope) {
    //common
    $scope.label = {
        type: "",
        simple: {
            name: "label teste"
        },
        grouped: {
            style: "input-group-lg",
            left: {
                show: true,
                name: "escreve ae"
            },
            right: {
                show: true,
                name: "agora sim"
            }
        }
    };
    var options = [];
    options.push("teste 1");
    options.push("teste 2");
    options.push("teste 3");
    options.push("teste 4");
    $scope.input = {
        type: "text",
        name: "teste",
        placeholder: "placeholder",
        options: options
    };
    $scope.labelInputStyle = {};
    //Use Case 1: Single Input - simple label
    $scope.inputCase1 = angular.copy($scope.input);
    $scope.labelCase1 = angular.copy($scope.label);
    $scope.labelCase1.type = "simple";
    //Use Case 2: Single Input - grouped label
    $scope.inputCase2 = angular.copy($scope.input);
    $scope.labelCase2 = angular.copy($scope.label);
    $scope.labelCase2.type = "grouped";
    //Use Case 3: Single Input - checkbox
    $scope.inputCase3 = angular.copy($scope.input);
    $scope.labelCase3 = angular.copy($scope.label);
    $scope.inputCase3.type = "checkbox";
    $scope.labelCase3.type = "simple";
    //Use Case 4: List Input - radio
    $scope.inputCase4 = angular.copy($scope.input);
    $scope.labelCase4 = angular.copy($scope.label);
    $scope.inputCase4.type = "radio";
    $scope.labelCase4.type = "simple";
    //Use Case 5: List Input  - autocomplete
    $scope.inputCase5 = angular.copy($scope.input);
    $scope.labelCase5 = angular.copy($scope.label);
    $scope.inputCase5.type = "autocomplete";
    $scope.labelCase5.type = "simple";
    //Use Case 6: List Input  - autocomplete
    $scope.inputCase6 = angular.copy($scope.input);
    $scope.labelCase6 = angular.copy($scope.label);
    $scope.inputCase6.type = "autocomplete";
    $scope.labelCase6.type = "grouped";
    //Use Case 7: List Input  - dropdown
    $scope.inputCase7 = angular.copy($scope.input);
    $scope.labelCase7 = angular.copy($scope.label);
    $scope.inputCase7.type = "dropdown";
    $scope.labelCase7.type = "simple";
    //Use Case 8: List Input  - dropdown
    $scope.inputCase8 = angular.copy($scope.input);
    $scope.labelCase8 = angular.copy($scope.label);
    $scope.inputCase8.type = "dropdown";
    $scope.labelCase8.type = "grouped";
    //Use Case 9: List Input  - dropdown key/value
    $scope.inputCase9 = angular.copy($scope.input);
    $scope.labelCase9 = angular.copy($scope.label);
    $scope.inputCase9.optionsType = "keyvalue";
    $scope.inputCase9.options = [];
    $scope.inputCase9.options.push({
        key: "t1",
        value: "teste1"
    });
    $scope.inputCase9.options.push({
        key: "t2",
        value: "teste2"
    });
    $scope.inputCase9.options.push({
        key: "t3",
        value: "teste3"
    });
    $scope.inputCase9.options.push({
        key: "t4",
        value: "teste4"
    });
    $scope.inputCase9.type = "dropdown";
    $scope.labelCase9.type = "simple";
    //Use Case 10: Single Input  - text area
    $scope.inputCase10 = angular.copy($scope.input);
    $scope.labelCase10 = angular.copy($scope.label);
    $scope.inputCase10.type = "textarea";
    $scope.labelCase10.type = "simple";
    //Use Case 11: Single Input  - password
    $scope.inputCase11 = angular.copy($scope.input);
    $scope.labelCase11 = angular.copy($scope.label);
    $scope.inputCase11.type = "password";
    $scope.labelCase11.type = "simple";
    //Use Case 12: List Input  - Checkbox
    $scope.inputCase12 = angular.copy($scope.input);
    $scope.labelCase12 = angular.copy($scope.label);
    $scope.inputCase12.type = "checkbox";
    $scope.labelCase12.type = "simple";
    //Use Case 13: Arrange Inputs
    $scope.input1 = angular.copy($scope.inputCase1);
    $scope.input2 = angular.copy($scope.inputCase4);
    $scope.input3 = angular.copy($scope.inputCase6);
    $scope.input4 = angular.copy($scope.inputCase9);
    $scope.input5 = angular.copy($scope.inputCase10);
    $scope.input6 = angular.copy($scope.inputCase12);
    $scope.label1 = angular.copy($scope.labelCase1);
    $scope.label2 = angular.copy($scope.labelCase4);
    $scope.label3 = angular.copy($scope.labelCase6);
    $scope.label4 = angular.copy($scope.labelCase9);
    $scope.label5 = angular.copy($scope.labelCase10);
    $scope.label6 = angular.copy($scope.labelCase12);
    $scope.inputsLabels = [];
    $scope.inputsLabels.push({
        input: $scope.input1,
        label: $scope.label1,
        labelInputStyle: "col-sm-",
        gridColSize: 4,
        type: "single"
    });
    $scope.inputsLabels.push({
        input: $scope.input2,
        label: $scope.label2 ,
        labelInputStyle: "col-sm-",
        gridColSize: 2,
        type: "list"
    });
    $scope.inputsLabels.push({
        input: $scope.input3,
        label: $scope.label3,
        labelInputStyle: "col-sm-",
        gridColSize: 6,
        type: "list"
    });
    $scope.inputsLabels.push({
        input: $scope.input4,
        label: $scope.label4,
        labelInputStyle: "col-sm-",
        gridColSize: 3,
        type: "list"
    });
    $scope.inputsLabels.push({
        input: $scope.input5,
        label: $scope.label5,
        labelInputStyle: "col-sm-",
        gridColSize: 9,
        type: "single"
    });
    $scope.inputsLabels.push({
        input: $scope.input6,
        label: $scope.label6,
        labelInputStyle: "col-sm-",
        gridColSize: 12,
        type: "list"
    });
    $scope.divStyle = "row";
}]);