app.controller('PanelsController', ['$scope', function($scope) {
    var title = {
        text: "TESTE",
        style: "panels-heading-text"
    };
    var glyphicons = [];
    glyphicons.push({
        show: true,
        style: "glyphicon-plus-sign glyphicon-heading-panel",
        onClick: {
            action: "goto",
            link: "http://getbootstrap.com/components/#panels"
        }
    });
    glyphicons.push({
        show: true,
        style: "glyphicon-pencil glyphicon-heading-panel",
        onClick: {
            action: "update",
        }
    });
    glyphicons.push({
        show: true,
        style: "glyphicon-remove glyphicon-heading-panel",
        onClick: {
            action: "delete",
        }
    });
    glyphicons.push({
        show: true,
        style: "glyphicon-chevron-up glyphicon-heading-panel",
        onClick: {
            action: "hide",
        }
    });
    glyphicons.push({
        show: false,
        style: "glyphicon-chevron-down glyphicon-heading-panel",
        onClick: {
            action: "show",
        }
    });
    var input = {
        type: "text",
        name: "teste",
        placeholder: "placeholder"
    };
    var label = {
        type: "grouped",
        simple: {
            name: "label teste"
        },
        grouped: {
            style: "input-group-lg",
            left: {
                show: true,
                name: "escreve ae"
            },
            right: {
                show: true,
                name: "o q? qq coisa"
            }
        }
    };
    var inputsLabels = [];
    inputsLabels.push({
        input: input,
        label: label,
        labelInputStyle: "col-sm-",
        gridColSize: 12,
        type: "single"
    });
    var divStyle = "row";
    var options = [];
    for (i = 0; i < 20; i++) {
        obj = {
            image: {
                id: i,
                url: "https://s3.amazonaws.com/Glambox.Catalog.Brand/Logo_275_SML.png",
                name: "name " + i,
                style: "img-brand-logo col-xs-4 col-sm-3 col-md-12",
                parent: {
                    style: "col-xs-1"
                }
            }
        };
        options.push(obj);
    }
    var selected = {};
    var tiles = {
        options: options,
        selected: selected
    };
    options = [];
    for (i = 0; i < 5; i++) {
        obj = {
            style: "card-style",
            image: {
                id: i,
                url: "https://s3.amazonaws.com/Glambox.Catalog.Brand/Logo_275_SML.png",
                name: "name " + i,
                style: "img-brand-logo",
                parent: {
                    style: "col-xs-1"
                }
            },
            subtitle: {
                text: "subtitle " + i
            },
            summary: {
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris rutrum metus non sagittis semper. Sed facilisis libero id libero dictum tincidunt. Proin eu velit eu risus eleifend tempor. Aenean in auctor neque. Vivamus porttitor sem ut congue scelerisque. Pellentesque non turpis laoreet, semper leo at, fringilla felis. Praesent gravida neque rutrum finibus vehicula.",
                style: "col-xs-11"
            }
        };
        options.push(obj);
    }
    var cards = {
        options: options,
        selected: selected
    };
    var header = [];
    header.push("CNPJ");
    header.push("Série");
    header.push("Número");
    header.push("Natureza");
    header.push("Data Emissão");
    header.push("Valor da Nota");
    var cnpjs = [];
    var series = [];
    var numeros = [];
    var naturezas = [];
    var datas = [];
    var valores = [];
    for (var i = 0; i < 5; i++) {
        cnpjs.push("CNPJ " + i);
        series.push("Série " + i);
        numeros.push("Número " + i);
        naturezas.push("Natureza " + i);
        datas.push("Data Emissão " + i);
        valores.push("Valor da Nota " + i);
    }
    var keyValues = [];
    keyValues.push({
        key: "CNPJ",
        values: cnpjs
    });
    keyValues.push({
        key: "Série",
        values: series
    });
    keyValues.push({
        key: "Número",
        values: numeros
    });
    keyValues.push({
        key: "Natureza",
        values: naturezas
    });
    keyValues.push({
        key: "Data Emissão",
        values: datas
    });
    keyValues.push({
        key: "Valor da Nota",
        values: valores
    });
    var bodies = [];
    bodies.push({
        show: true,
        style: "",
        type: "text",
        content: "TESTE"
    });
    bodies.push({
        show: true,
        style: "",
        type: "inputs",
        content: {
            divStyle: divStyle,
            inputsLabels: inputsLabels
        }
    });
    bodies.push({
        show: true,
        style: "",
        type: "tiles",
        content: {
            type: "default",
            tiles: tiles,
            parentStyle: "row"
        }
    });
    bodies.push({
        show: true,
        style: "",
        type: "cards",
        content: {
            type: "default",
            cards: cards,
            parentStyle: "row"
        }
    });
    bodies.push({
        show: true,
        style: "",
        type: "tables",
        content: {
            header: header,
            keyValues: keyValues,
            widthBreak: 768
        }
    });
    $scope.heading = {
        show: true,
        style: "row",
        title: title,
        glyphicons: glyphicons
    };

    $scope.parentStyle = "row";
    $scope.panel = {
        style: "panel-primary col-xs-12",
        show: true
    };

    $scope.panel1 = angular.copy($scope.panel);
    $scope.panel2 = angular.copy($scope.panel);
    $scope.panel3 = angular.copy($scope.panel);
    $scope.panel4 = angular.copy($scope.panel);
    $scope.panel5 = angular.copy($scope.panel);
    $scope.panel6 = angular.copy($scope.panel);
    $scope.panel7 = angular.copy($scope.panel);
    //Use Case 1
    $scope.heading0 = angular.copy($scope.heading);

	// Use Case 2
    $scope.body1 = {
        show: true,
        style: "row body-style",
        bodies: bodies.slice(0,1)
    };
    $scope.heading1 = angular.copy($scope.heading);

	// Use Case 3
    $scope.body2 = {
        show: true,
        style: "row body-style",
        bodies: bodies.slice(1,2)
    };

	// Use Case 4
    $scope.body3 = {
        show: true,
        style: "row body-style",
        bodies: bodies.slice(2,3)
    };

	// Use Case 5
    $scope.body4 = {
        show: true,
        style: "row body-style",
        bodies: bodies.slice(3,4)
    };

	// Use Case 6
    $scope.body5 = {
        show: true,
        style: "row body-style",
        bodies: bodies.slice(4,5)
    };

    // Use Case 7
    $scope.body = {
        show: true,
        style: "row body-style",
        bodies: bodies
    };
}]);