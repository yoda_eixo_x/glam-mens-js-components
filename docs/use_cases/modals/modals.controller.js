app.controller(
    'ModalController',
    [
        '$scope',
        function( $scope ) {
            $scope.count = 0;

            $scope.hello = 'Hello';

            $scope.addCount = function() {
                $scope.count += 1;
            };

            $scope.setHello = function(data) {
                $scope.hello = data;
            };

            $scope.action = {
                text: 'Toggle The Modal!',
                action: {
                    name: 'modal',
                    execute: function() {
                        $scope.modalParams.toggle = ( $scope.modalParams.toggle )
                            ? false : true;
                    }
                },
                buttonStyle: 'btn btn-warning'
            };

            $scope.modalParams = {
                toggle: false,
                header_title: 'Modal Window Test',
                header_show: true,
                header_close_button: true,
                footer_show: true,
                footer_close_button: true
            };
        }
    ]
);