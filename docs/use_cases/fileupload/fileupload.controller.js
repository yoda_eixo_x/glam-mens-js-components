app.controller(
    'FileUploadController',
    [
        '$scope',
        function( $scope ) {
            $scope.singleCase1 = {
                type: 'single',
                url: 'http://localhost:8080/com.eixox.api/nfe/UploadXml',
                headers: {
                    'Authorization': '07ba157a-ce6c-48ac-b023-1836fca61a6b'
                }
            };
            
            $scope.singleCase2 = {
                type: 'single',
                url: 'http://localhost:8080/com.eixox.api/nfe/UploadXml',
                headers: {
                    'Authorization': '07ba157a-ce6c-48ac-b023-1836fca61a6b'
                },
                accept: '.pdf'
            };
            
            $scope.multipleCase1 = {
                type: 'multiple',
                url: 'http://localhost:8080/com.eixox.api/nfe/UploadXml',
                headers: {
                    'Authorization': '07ba157a-ce6c-48ac-b023-1836fca61a6b'
                }
            };
            
            $scope.multipleCase2 = {
                type: 'multiple',
                url: 'http://localhost:8080/com.eixox.api/nfe/UploadXml',
                headers: {
                    'Authorization': '07ba157a-ce6c-48ac-b023-1836fca61a6b'
                },
                accept: 'image/*',
                maxsize: 204800
            };
            
            this.$doCheck = function()
            {
                if ( $scope.singleCase1.finishedList && $scope.singleCase1.finishedList.length > 0 ) {
                    console.log($scope.singleCase1.finishedList);
                } else {
                    console.log('Uploaded list is empty.');
                }
                
                if ( $scope.singleCase1.uploadList && $scope.singleCase1.uploadList.length > 0 ) {
                    console.log($scope.singleCase1.uploadList);
                } else {
                    console.log('Upload queue is empty.');
                }
            };
        }
    ]
);