app.controller(
    'PaginateTestController',
    [
        '$scope',
        '$routeParams',
        function( $scope, $routeParams ) {
            // Pagination parameters
            $scope.params1 = {
                current: $routeParams.page || 1,
                page_size:  10,
                data_size:  145,
                wrapper_class:  'btn-group',
                default_class:  'btn btn-default',
                current_class:  'btn btn-default active',
                show_arrows: true,
                show_numbers: true,
                number_side: 2,
                page_url: 'usecases/paginate/',
                arrow_first_text: "Primeira",
                arrow_prev_text: "Anterior",
                arrow_next_text: "Próxima",
                arrow_last_text: "Última"
            };

            $scope.params2 = {
                current: $routeParams.page || 1,
                page_size:  10,
                data_size:  145,
                wrapper_class:  'btn-group',
                default_class:  'btn btn-default',
                current_class:  'btn btn-default active',
                show_arrows: true,
                show_numbers: true,
                number_side: 2,
                page_url: 'demo',
                arrow_first_text: "Primeira",
                arrow_prev_text: "Anterior",
                arrow_next_text: "Próxima",
                arrow_last_text: "Última"
            };

            /**
             * Toggles base navigation on/off.
             */
            $scope.toggleArrows = function()
            {
                $scope.params2.show_arrows = ( $scope.params2.show_arrows === true ) ? false : true;
            };

            /**
             * Toggles numbered navigation on/off.
             */
            $scope.toggleNumbers = function()
            {
                $scope.params2.show_numbers = ( $scope.params2.show_numbers === true ) ? false : true;
            };
        }
    ]
);