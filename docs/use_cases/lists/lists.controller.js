app.controller('ListsController', ['$scope', function($scope) {
    var obj, i;
    // Basic data to be used in use cases for tiles
    $scope.tiles = {
        options: [],
        selected: {}
    };
    for (i = 0; i < 5; i++) {
        obj = {
            style: "",
            image: {
                id: i,
                url: "https://s3.amazonaws.com/Glambox.Catalog.Brand/Logo_275_SML.png",
                name: "name " + i,
                style: "img-brand-logo",
                goto: "https://s3.amazonaws.com/Glambox.Catalog.Brand/Logo_275_SML.png"
            },
            subtitle: {
                text: "subtitle " + i,
                style: ""
            }
        };
        $scope.tiles.options.push(obj);
    }
    $scope.tileFilter = $scope.tiles[2];
    // Use Case Tiles 1 - only image
    $scope.tiles1 = angular.copy($scope.tiles);
    //Use Case Tiles 2 - with subtitles
    $scope.tiles2 = angular.copy($scope.tiles);
    //Use Case Tiles 3 - goto action and filter
    $scope.tiles3 = angular.copy($scope.tiles);
    //Use Case Tiles 4 - fluid/responsiveness
    $scope.parentStyle = "row";
    $scope.tiles4 = angular.copy($scope.tiles);
    for (i = 0; i < 5; i++) {
        $scope.tiles4.options[i].style = "col-xs-4 col-sm-3 col-md-12";
    }

    // Basic data to be used in use cases for cards
    $scope.cards = {
        options: [],
        selected: {}
    };
    for (i = 0; i < 5; i++) {
        obj = {
            style: "card-style",
            image: {
                id: i,
                url: "https://s3.amazonaws.com/Glambox.Catalog.Brand/Logo_275_SML.png",
                name: "name " + i,
                style: "img-brand-logo",
                parent: {
                    style: "col-xs-1"
                }
            },
            subtitle: {
                text: "subtitle " + i
            },
            seemore: {
                style: "btn-primary seemore-button",
                goto: "https://s3.amazonaws.com/Glambox.Catalog.Brand/Logo_275_SML.png",
                text: "Ver Mais..."
            },
            summary: {
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris rutrum metus non sagittis semper. Sed facilisis libero id libero dictum tincidunt. Proin eu velit eu risus eleifend tempor. Aenean in auctor neque. Vivamus porttitor sem ut congue scelerisque. Pellentesque non turpis laoreet, semper leo at, fringilla felis. Praesent gravida neque rutrum finibus vehicula.",
                style: "col-xs-11"
            }
        };
        $scope.cards.options.push(obj);
    }
    //Use Case Cards 1 - left image without subtitles
    $scope.cards1 = angular.copy($scope.cards);
    //Use Case Cards 2 - left image with subtitles
    $scope.cards2 = angular.copy($scope.cards);
    //Use Case Cards 3 - goto button action
    $scope.cards3 = angular.copy($scope.cards);
}]);