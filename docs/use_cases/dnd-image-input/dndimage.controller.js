app.controller(
    'DndImageController',
    [
        '$scope',
        '$http',
        function( $scope, $http ) {
            $scope.imageFileList = [];
            $scope.imageFile = null;
            $scope.imageView = '';

            $scope.clickitty = function()
            {
                if ($scope.imageFileList[0]) {
                    var xhr = new XMLHttpRequest();
                    var formData = new FormData();
                    for (var n in $scope.imageFileList) {
                        var curr = $scope.imageFileList[n];
                        formData.append('userFile', curr);
                        //formData.append('uploadedFile', curr);
                        xhr.addEventListener('load', function (e) {
                            console.log(e);
                            $scope.imageFileList = [];
                        });
                        xhr.open(
                            'POST',
                            //'http://10.1.1.192:80/'
                            'http://localhost:3000/api/load/'
                        );
                        xhr.send(formData);
                    }
                }
            };

            this.$doCheck = function()
            {
                if ($scope.imageFile !== null) {
                    $scope.imageFileList.push($scope.imageFile[0]);
                    $scope.imageFile = null;
                }
            };
        }
    ]
);